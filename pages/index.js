import React, { Component } from 'react';
import fetch from 'isomorphic-unfetch';
import Layout from '../components/Layout';
import Todos from '../components/Todos';
import AddTodo from '../components/AddTodo';
import EditTodo from '../components/EditTodo';
import SearchTodo from '../components/SearchTodo';

class Index extends Component {
  state = {
    todos: [
    ],
    editMode: false,
    todoToEdit: {
      id: '',
      title: ''
    }
  }

  componentDidMount() {
    this.setState({ todos : [...this.props.data]});
  }
  
  addTodo = (title) => {
    fetch('https://jsonplaceholder.typicode.com/todos', {
      method: 'POST',
      body: JSON.stringify({
        title,
        completed: false
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
    .then((response) => response.json())
    .then((data) => { 
      data.id = String(Math.floor(Math.random() * 10000));
      return this.setState({ todos : [data, ...this.state.todos]})
      }
    );
  }

  completeTodo = (id) => {
    this.setState({ todos: this.state.todos.map(todo => {
      if(todo.id === id) {
        todo.completed = !todo.completed
      }
      return todo;
    })})
  }

  deleteTodo = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      method: 'DELETE',
    })
    .then(res => res.json())
    .then(data => this.setState({ todos: [...this.state.todos.filter((todo) => { return todo.id !== id})]}))
  }

  searchTodo = (title) => {
    fetch('https://jsonplaceholder.typicode.com/todos?_limit=8')
      .then((response) => response.json())
      .then((data) => this.setState({ todos: [...data.filter((todo) => {
        const regex = new RegExp(`${title}`, 'gi');
        return todo.title.match(regex)
      })]}));
  }

  activateEdit = (id, title) => {
    this.setState({ todoToEdit: {id, title} });
    this.setState({ editMode: true });
  }

  cancelEdit = () => {
    this.setState({ editMode: false });
    this.setState({ todoToEdit: {id: '', title: ''} });
  }

  editTodo = (title) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
      method: 'PUT',
      body: JSON.stringify({
        title
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
    .then((response) => response.json())
    .then((data) => {
      this.setState({ todos: [...this.state.todos.map(todo => {
        if(todo.id === this.state.todoToEdit.id) {
          todo.title = title;
          return todo
        }
        return todo
      })]})
    });

    this.setState({ editMode: false });
  }

  render() {
    return (
      <Layout>
      <div className="main-content">
        {!this.state.editMode ?
          <AddTodo addTodo={this.addTodo}/>:
          <EditTodo todoToEdit={this.state.todoToEdit} editTodo={this.editTodo} cancelEdit={this.cancelEdit}/>
        }
        
        <SearchTodo searchTodo={this.searchTodo}/>
        <ul className="list-group">
          <h3 className="tasks-title">Not Completed Tasks</h3>
          <Todos todos={this.state.todos.filter(todo => todo.completed === false)} completeTodo={this.completeTodo} deleteTodo={this.deleteTodo} activateEdit={this.activateEdit} isDisabled={this.state.editMode}/>
          <h3 className="tasks-title">Completed Tasks</h3>
          <Todos todos={this.state.todos.filter(todo => todo.completed === true)} completeTodo={this.completeTodo} deleteTodo={this.deleteTodo} activateEdit={this.activateEdit} isDisabled={this.state.editMode}/>
        </ul>
        <p className="signature">This software has been developed by Carlos Delgado. <br/> For more information click <a href="https://delgadociteriowa.github.io/main/" target="_blank">here.</a></p>
      </div>
    </Layout>
    )
  }
}

Index.getInitialProps = async function() {
  const res = await fetch('https://jsonplaceholder.typicode.com/todos?_limit=10');
  const data = await res.json();
  return {
    data
  }
}

export default Index;