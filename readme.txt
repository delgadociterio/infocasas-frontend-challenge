Infocasas Frontend Challenge

Developed by: Carlos Delgado
  Website: https://delgadociteriowa.github.io/main/
  Linkedin Profile: https://www.linkedin.com/in/delgadociterio/
  Behance Gallery: https://www.behance.net/delgadociterio

To execute this software, follow the next steps:

In your terminal, in the root of this folder, run the following command:

  - npm install.

When everything is installed run the following command:

  - npm run dev

Then, go to the following website with your favorite internet browser:

  - http://localhost:3000/

.
