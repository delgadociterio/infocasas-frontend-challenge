import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Todo from './Todo';

class Todos extends Component {
  render() {
    return (
      <Fragment>
        {this.props.todos.map(
          (todo) => (<Todo key={todo.id} todo={todo} completeTodo={this.props.completeTodo} deleteTodo={this.props.deleteTodo} activateEdit={this.props.activateEdit} isDisabled={this.props.isDisabled}/>)
        )}
      </Fragment>
    )
  }
}


Todos.propTypes = {
  todos: PropTypes.array.isRequired,
  completeTodo: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  activateEdit: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool.isRequired
}

export default Todos;