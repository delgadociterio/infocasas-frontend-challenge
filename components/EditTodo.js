import React, { Component } from 'react'

export class EditTodo extends Component {
  state = {
    title: this.props.todoToEdit.title
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.editTodo(this.state.title);
    this.setState({ title: ''});
  }

  onChange = (e) => this.setState({ title: e.target.value})

  render() {
    return (
      <div className="jumbotron jumbotron-fluid jumbo-form">
        <h2>Edit task</h2>
        <form onSubmit={this.onSubmit} className="add-todo-form">
          <input className="add-todo" type="text" name="title" value={this.state.title} onChange={this.onChange}/>
          <input className="btn btn-success submit-todo" type="submit" value="Edit"/>
        </form>
        <button className="btn btn-primary btn-lg cancel-edition-btn" onClick={this.props.cancelEdit}>Cancel Edition</button>
      </div> 
    )
  }
}

export default EditTodo
