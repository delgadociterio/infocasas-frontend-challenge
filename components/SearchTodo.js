import React, { Component } from 'react';

export class SearchTodo extends Component {
  state = {
    title: ''
  }

  onChange = (e) => {
    this.setState({ title: e.target.value});
    this.props.searchTodo(e.target.value);
  };

  render() {
    return (
      <div className="jumbotron jumbotron-fluid jumbo-search">
          <input className="search-todo" type="text" name="title" value={this.state.title} onChange={this.onChange} placeholder="Search..."/>
      </div> 
    )
  }
}

export default SearchTodo